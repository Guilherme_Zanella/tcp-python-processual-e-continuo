import socket
import threading

Client = []

def rodaThread(conn):
    while True:

        print('Waiting for messages')
        data = conn.recv(1024)
        
        print('Received {} bytes from {}'.format(len(data), conn.getpeername()))
        
        # Return message
        conn.send(data)
    
        for i in Client:
            if i != conn:
                i.send(data)
    
    conn.close()
    return

        # Host/Port
def Main():
    host = "0.0.0.0"
    port = 10000
    socketTCP = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    socketTCP.bind((host,port))
    socketTCP.listen(1)
    print('Server TCP: {}:{}'.format(host,port))

    
    while True:
        # Client connection
        conn, addr = socketTCP.accept()
        Client.append(conn)

        print ("Connection made by: " + str(addr))
        
        # Execution thred
        t = threading.Thread(target=rodaThread, args=(conn,))
        t.start()

    socket.close()

if __name__ == '__main__':
    Main()
