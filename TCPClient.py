import socket
import threading

def rodaThread(mySocket):    

    while True:      
       
        # receive message from server
        data = mySocket.recv(1024)
        # user @ mesangem
        msg = data.decode().split("@")

        if(msg[0] == nome):
            print ('')
        else:
            print('{} - {}'.format( msg[0],msg[1]))     

    mySocket.close()
    return

def Main():
    host = '127.0.0.1'
    port = 10000

    #client socket
    mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    #connection server
    mySocket.connect((host,port))

    t = threading.Thread(target=rodaThread, args=(mySocket,))
    t.start()

    #Client name
    nome = input("ClientName: ")

    #waiting message
    message = input(" -> (Q get out) ")

    while message != 'q' and message:
        message = nome + "@" + message
        # send server message
        mySocket.send(message.encode())   

        #waiting message
        message = input(" -> (Q get out) ")


if __name__ == '__main__':
    Main()
